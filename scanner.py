import argparse
from socket import *
from threading import *

screen_lock = Semaphore(value=1)

def connect(host, port):
    try:
        conn_skt = socket(AF_INET, SOCK_STREAM)
        conn_skt.connect((host,port))
        conn_skt.send("beep\r\n")
        result = conn_skt.recv(100)
        screen_lock.acquire()
        print "[+]TCP Port %d OPEN"%port
        print "[+] " + str(result) + "\n"
    except:
        screen_lock.acquire()
        print "[-]TCP Port %d CLOSED/FILTERED\n"%port
    finally:
        screen_lock.release()
        conn_skt.close()

def port_scan(host,ports):
    try:
        getIP = gethostbyname(host)
    except:
        print "[-]Cannot resolve unknown hostname!!"
        return
    try:
        getName = gethostbyaddr(getIP)
        print "[*]Scanning: " + getName[0]
    except:
        print "[*]Scanning: " + getIP
    setdefaulttimeout(1)
    
    for port in ports:
        t = Thread(target=connect, args=(host,int(port)))
        t.start()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('host', type=str, action='store',help='Host address')
    parser.add_argument('port', type=str, action='store',help='Port number')
    args = parser.parse_args()

    t_host = args.host
    t_port = str(args.port).split(',')

    port_scan(t_host,t_port)

if __name__ == "__main__":
    main()

