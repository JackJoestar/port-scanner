# DISCLAIMER
This program is intended for educational purposes. Please don't use it to scan restricted networks and systems, and don't hold me responsible for your actions.
Thank you!!

### HOWTO
To use this program, you must provide a host in IPv4 format and port(s) to scan.

`python scanner.py 127.0.0.1 80`

### Output
[*]Scanning: localhost <br/>
[+]TCP Port 22 OPEN <br/>
[+] SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.1 <br/>
